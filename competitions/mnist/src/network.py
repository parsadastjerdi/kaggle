'''
    Code was written while reading the textbook from neuralnetworksanddeeplearning.com.
    Implemented using Python 3.7.2.
    @author{Michael Nielson}
    @author{Parsa Dastjerdi}
    @copyright{}
    @license{}
'''

import numpy as np
import random

from keras.datasets import mnist

def sigmoid(x):
    '''
    Overview:
        Sigmoid used as the activation function for the neural network
    '''
    return 1.0 / (1.0 + np.exp(-1.0 * x))

def sigmoid_prime(x):
    '''
    Overview:
        Derivative of the sigmoid function
    '''
    return sigmoid(x) * (1 - sigmoid(x))


class Network:
    '''
        Overview:
            A simple neural network class
    '''

    def __init__(self, sizes):
        '''
            All biases and weights are initialized randomly. 
            User provides number of layers and number of nodes within each layer through the sizes parameter
            Assumes that the first layer of neurons is the input layer and doesn't set any biases for that layer
        '''
        self.num_layers = len(sizes)
        self.sizes = sizes
        self.biases = [np.random.rand(y, 1) for y in sizes[1:]]
        self.weights = [np.random.randn(y, x) for x, y in zip(sizes[:-1], sizes[1:])]


    
    def feedforward(self, a):
        '''
            Output:
                Returns an output for a given input
        '''
        for b, w in zip(self.biases, self.weights):
            a = sigmoid(np.dot(w,a) + b)
        return a



    def sgd(self, training_data, epochs, mini_batch_size, eta, test_data=None):
        '''
        Overview:
            Train the neural network using stochastic gradient descent
        Input:
            training_data: list of tuples (x,y) representing the training inputs and corresponding desired outputs
            epochs: number of epochs
            mini_batch_size: size of the mini batch
            eta: learning rate
            test_data: test data to evaluate network after each epoch 
        '''

        if test_data:
            n_test = len(test_data)

        training_data = list(training_data)
        n = len(training_data)

        # randomly shuffle training data and then split into mini batches - allows random sampling
        for j in range(epochs):
            random.shuffle(training_data)
            mini_batches = [training_data[k : k + mini_batch_size]
                                for k in range(0, n, mini_batch_size)]

            # apply a single 
            for mini_batch in mini_batches:
                self.update_mini_batch(mini_batch, eta)
            
            if test_data:
                print("Epoch {0}: {1} / {2}".format(j, self.evaluate(test_data), n_test))
            else:
                print("Epoch {0} complete".format(j))
            


    def update_mini_batch(self, mini_batch, eta):
        '''
        Overview:
            Update the network's weights and biases by applying gradient descent w/ back propagation
            on a single mini batch.
        Input:
            mini_batch: list of tuples (x,y)
            eta: learning rate
        Output:

        '''

        nabla_b = [np.zeros(b.shape) for b in self.biases]
        nabla_w = [np.zeros(w.shape) for w in self.weights]

        print(type(mini_batch))
        print(len(mini_batch))
        for x in mini_batch:
            print(type(x))

        for (x, y) in mini_batch:
            delta_nabla_b, delta_nabla_w = self.backprop(x, y)
            nabla_b = [nb + dnb for nb, dnb in zip(nabla_b, delta_nabla_b)]
            nabla_w = [nw + dnw for nw, dnw in zip(nabla_w, delta_nabla_w)]
        
        self.weights = [w - (eta / len(mini_batch)) * nw 
                        for w, nw in zip(self.weights, nabla_w)]

        self.biases = [b - (eta / len(mini_batch)) * nb
                        for b, nb in zip(self.biases, nabla_b)]



    def backprop(self, x, y):
        '''
        Overview:
            Performs back propagation on the 
        Input:
            activation:
            y: 
        Output:
            Returns a tuple (nabla_x, nabla_y) representing the gradient of the cost function Cx.
        '''

        nabla_b = [np.zeros(b.space) for b in self.biases]
        nabla_w = [np.zeros(w.space) for w in self.weights]

        # feedforward
        activation = x
        activations = [x] # list to store all the activations layer by layer
        # activations = [activation] # replace x with activation
        zs = [] # list to store all the z vectors, layer by layer

        for b, w in zip(self.biases, self.weights):
            z = np.dot(w, activation) + b
            zs.append(z)
            activation = sigmoid(z)
            activations.append(activation)
            # activations.append(sigmoid(z)) #  replace two lines above with this

        # backward pass
        delta = self.cost_derivative(activations[-1], y) * sigmoid_prime(zs[-1])
        nabla_b[-1] = delta
        nabla_w[-1] = np.dot(delta, activations[-2].transpose())

        for l in range(2, self.num_layers):
            z = zs[-l]
            sp = sigmoid_prime(z)
            delta = np.dot(self.weights[1 - l].transpose(), delta) * sp
            nabla_b[-l] = delta
            nabla_w[-l] = np.dot(delta, activations[-1 - l].transpose())

        return (nabla_b, nabla_w)



    def evaluate(self, test_data):
        '''
        Overview:
        Input:
        Output:
        '''

        test_results = [(np.argmax(self.feedforward(x)), y) for (x, y) in test_data]
        return sum(int(x == y) for (x, y) in test_results)



    def cost_derivative(self, output_activations, y):
        '''
        Overview:
            Return a vector of partial derivatives (partial Cx)
        Input:
        Output:
        '''

        return (output_activations - y)


# test the neural network
if __name__ == '__main__':
    (train_images, train_labels), (test_images, test_labels) = mnist.load_data()
    net = Network([784,30,10])
    # net.sgd((train_images, train_labels), 30, 10, 3.0, test_data=(test_images, test_labels)))
    print(train_images.shape)

